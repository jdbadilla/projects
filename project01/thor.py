#!/usr/bin/env python2.7

import time
import datetime
import os
import socket
import sys
import getopt
import logging
import re

# Global Variables

REQUESTS=1
PROCESSES=1
URL=''
HOST=''
ADDRESS=''
PATH='/'
LOGLEVEL=logging.INFO
PORT=80

# usage function

def usage(usage_status=0):
	print '''Usage: thor.py [-r REQUESTS -p PROCESSES -v] URL

Options:

    -h           Show this help message
    -v           Set logging to DEBUG level

    -r REQUESTS  Number of requests per process (default is 1)
    -p PROCESSES Number of processes (default is 1)'''
	sys.exit(usage_status)

# check URL input

if len(sys.argv) == 1:
	sys.exit("ERROR: At least the URL must be provided as the last argument!")

if (len(sys.argv) > 1):
	URL = str(sys.argv[-1])

URL = sys.argv[-1]

# Parse command line arguments with getopt
try:
	options, remainder = getopt.getopt(sys.argv[1:],'hvr:p:')
except getopt.GetoptError as e:
	print e
	usage(1)

for opt, arg in options:
	if opt == '-h':
		usage(1)
	elif opt == '-v':
		LOGLEVEL = logging.DEBUG
	elif opt == '-r':
		if not arg.isdigit():
			sys.exit("ERROR: Option -r needs an argument!")
		else:
			REQUESTS = arg
	elif opt == '-p':
		if not arg.isdigit():
			sys.exit("ERROR: Option -p needs an argument!")
		else:
			PROCESSES = arg
	else:
		usage(1)

# TCPClient class
class TCPClient(object):
	def __init__(self,address=ADDRESS,port=PORT):
		self.logger	 =	logging.getLogger()
		self.address =	socket.gethostbyname(address)
		self.port	 =	port
		self.socket	 =	socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	def run(self):
		try:
			self.socket.connect((self.address, self.port))
			# Connect to server and create a file object
			self.stream = self.socket.makefile('w+')
		except socket.error as e:
			self.logger.error('Could not connect to {}:{}: {}'.format(self.address, self.port, e))
			sys.exit(1)

		# Display DEBUGGING information
		self.logger.debug('URL: {}'.format(URL))
		self.logger.debug('HOST: {}'.format(HOST))
		self.logger.debug('PORT: {}'.format(self.port))
		self.logger.debug('PATH: {}'.format(PATH))
		self.logger.debug('Connected to...{}:{}...'.format(self.address,self.port))

		# Send the request, Print the response
		try:
			self.logger.debug('Sending request...')
			self.socket.sendall(REQUEST_STR)
			self.logger.debug('Receiving response...')
			print self.socket.recv(4096)

		except Exception as e:
			self.logger.exception('Exception: {}'.format(e))
		finally:	# "finally" --> gets run even if there's an exception
			self.finish()

	def finish(self):
		# Finish connection
		self.logger.debug('Finishing...')
		try:
			self.socket.shutdown(socket.SHUT_RDWR)
		except socket.error:
			pass
		finally:
			self.socket.close()

# EchoClient class 
class HTTPClient(TCPClient):
	def handle(self):
		# Handles connection by reading data obtained from TCP client and writes it back until EOF #
		self.logger.debug('Handling...')
		try:
			data = sys.stdin.readline()
			while data:
				# Send STDIN to server
				self.stream.write(data)
				self.stream.flush()	# don't forget to flush ;)	

				# Read from Server to STDOUT
				data = self.stream.write(data)
				sys.stdout.write(data)

		except socket.error:
			pass	# "pass" --> Ignores specified errors (all types, if except)

# Main execution
if __name__ == '__main__':

	# Setting up logging level
	logging.basicConfig(
		level = LOGLEVEL,
		format = '[%(asctime)s] %(message)s',
		datefmt = '%Y-%m-%d %H:%M:%S',
	)

	# Get host address and make request string
	try:
		# parsing the URL
		possible_patterns = '(?:http.*://)?(?P<host>[^:/ ]+).?(?P<port>[0-9]*).*'
		match = re.search(possible_patterns,URL)
		HOST = match.group('host')
		PORT = match.group('port')
		REQUEST_STR = ('GET /index.html HTTP/1.1\r\n' + 'Host: ' + HOST + '\r\n' + '\r\n')
		ADDRESS = socket.gethostbyname(HOST)
	except socket.gaierror as e:	# gai = Given Address Info (exception if host name is invalid)
		logging.error('Unable to lookeup {}: {}'.format(ADDRESS, e))
		sys.exit(1)

	# Instantiate and run client, using fork for different processes
	for process in range(int(PROCESSES)):
		total_time = 0
		for request in range(int(REQUESTS)):
			start_time = time.time()
			client = HTTPClient(ADDRESS)

			try:
				pid = os.fork()	# create instantiation of running own process (child)
			except OSError as e:
				print >> sys.stderr, 'Unable to fork: {}'.format(e)
				sys.exit(1)

			if pid == 0: # Child
				try:
					client.run()
				except KeyboardInterrupt:
					sys.exit(0)
				sys.exit(0)

			else:	# Parent
				pid, status = os.wait()
				end_time = time.time()
				duration = end_time-start_time
				client.logger.debug('{} | Elapsed time: {:.2f} seconds'.format(pid,duration))
				total_time += duration
		average_time = float(total_time)/(request+1)
		client.logger.debug('{} | Average elapsed time: {:.2f} seconds'.format(pid, average_time))
	client.logger.debug('Process {} terminated with exit status {}'.format(pid, status))