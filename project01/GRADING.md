Project 01 - Grading
====================

**Score**: 0 / 20

Deductions
----------

* Thor

* Spidey
    - 0.5   Hangs on certain tests
    - 0.25  Improper HTTP error status
    - 1.0   Does not implement forking

* Report

    - 0.25  Report does not build
    - 1.0   No graphs or plots
    - 0.5   Missing measurements

Comments
--------
    
* Improper collaboration with another group
