#!/usr/bin/env python2.7
# mini-hulk --> random password generator
import string, os, sys, random, getopt
import itertools, hashlib

# Utility Function
def usage(exit_code=0):
    print >>sys.stderr, '''Usage: hulk.py [-a ALPHABET -l LENGTH -s HASHES -p PREFIX]

Options:

      -a  ALPHABET    Alphabet used for passwords
      -l  LENGTH      Length for passwords
      -s  HASHES      Path to file containing hashes
      -p  PREFIX      Prefix to use for each candidate password'''
    sys.exit(exit_code)

#Constants
ALPHABET = string.ascii_lowercase + string.digits
LENGTH	 = 8
HASHES	 = "hashes.txt"
PREFIX   = ""

#Getopts
try:
	options, remainder = getopt.getopt(sys.argv[1:],"a:l:s:p:")
except getopt.GetoptError as e:
	print e
	usage(1)

for opt, arg in options:
	if opt == "-a":
		ALPHABET = arg
	elif opt == "-l":
		LENGTH = int(arg)
	elif opt == "-s":
		HASHES = arg
	elif opt == "-p":
		PREFIX = arg
	else:
		usage(1)

def md5sum(s): # Compute MD5 hash of string
	return hashlib.md5(s).hexdigest()

# Main execution

if __name__ == '__main__':
	hashes = set([l.strip() for l in open(HASHES)]) # removes leading spaces
	found = set()

	#for x in range (1,LENGTH): why does this not work?
	for candidate in itertools.product(ALPHABET, repeat=LENGTH):
		candidate = ''.join(candidate)
		candidate = PREFIX + candidate # adds the PREFIX to the beginning of the string
		checksum = md5sum(candidate)
		if checksum in hashes:
			found.add(candidate)

	for word in sorted(found):
		print word
