Project 02: Distributed Computing
=================================

1. 'hulk.py' uses brute-force to crack a set of MD5 hashes. Given an alphabet and a string of digits from 0-9, it will compute every such permutation up to a specified length and checks if it is in the set of hashes. The program also uses whatever PREFIX is specified to the beggining of any alphanumeric string that we generate. It uses a getopt implementation to parse the command line arguments and a function imported from the hashlib to compute the hash for any string that we give it. It then compares the hash that it found to a file that we specify to see if the randomly generated alphanumeric string matches any of the hashes in the given file.

2. 'Fury.py' is a program that utilizes hulk.py to crack passwords using distributive computing. Fury is a program that gives input arguments to hulk. Fury sends requests to hulk.py to crack any password up to length 6, after that, it adds a prefix to the hulk.py script in order to maximie the speed of the script over condor, the distributive computing network. Additionally, fury.py stores a log that keeps track of what commands utilizing hulk.py have been attempted. This way, if you were to run fury.py again, it would not try to run the same commands again. Each of these commands is sent to the worker_queue and then forwarded to be ran using submit_task(). This way, all of the commands can be sent to the queue and divided up among the workers. If there is a failure, you can just run fury.py again and it will skip previously used commands. 

We tested Fury.py by outputting the commands used, only looping through a few different combinations to ensure accuracy before submitting the entire script.

- It is much harder to brute-force longer passwords rather than shorter passwords that use different (or additional) symbols and characters. In this case, the complexity is 36^n (where n is the length of the password).

