#!/usr/bin/env python2.7

import sys, work_queue, getopt, string, itertools
from random import randint

#Constansts

ALPHABET = string.ascii_lowercase + string.digits
HASHES = "-s hashes.txt"
hashSource = "hashes.txt"
SOURCES = ('hulk.py', hashSource)
randPort = randint(0,999)
PORT = 9000+randPort # make random port assignment
JOURNAL = {}

#Main Execution

if __name__ == '__main__':
	queue = work_queue.WorkQueue(PORT, name='fury-msills', catalog=True)
	queue.specify_log('fury.log')

#	for _ in range(TASKS):
#	for LENGTH in range (1,8):
	for LENGTH in range (1,3):
		if LENGTH >= 7: #6
			prefix = LENGTH - 6 #5
			for PREFIX in itertools.product(ALPHABET, repeat=prefix):
				PREFIX = ''.join(PREFIX)
				PREFIX = "-p " + PREFIX
		else:
			PREFIX = ""
		command = './hulk.py {} {} {}'.format(LENGTH, HASHES, PREFIX)
		#command = 'hulk.py {} {} {}'.format(LENGTH, HASHES, PREFIX)
		if command in JOURNAL:
    			print >>sys.stderr, 'Already did', command
		task = work_queue.Task(command)
			
		for source in SOURCES:
			task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)
				
		queue.submit(task)
		
	while not queue.empty():
		task = queue.wait()
		if task and task.return_status == 0: #task successfully ran, data integrity
			sys.stdout.write(task.output)
			## Example recording
			JOURNAL[task.command] = task.output.split()
			with open('journal.json.new', 'w') as stream:  # rewrites my journal
		    		json.dump(JOURNAL, stream) #JOURNAL is a dict
			os.rename('journal.json.new', 'journal.json')

